#!/bin/sh

curl -d @- http://localhost:8080/alerts <<EOF
{
  "receiver": "user-alerts-adapter",
  "status": "resolved",
  "alerts": [
    {
      "status": "resolved",
      "labels": {
        "alertname": "UserVolumeUsageAlert",
        "endpoint": "https-metrics",
        "instance": "188.184.103.222:10250",
        "job": "kubelet",
        "metrics_path": "/metrics",
        "namespace": "test-pvc-alert",
        "node": "standard-56m6q",
        "persistentvolumeclaim": "data-pvc",
        "prometheus": "openshift-monitoring/k8s",
        "service": "kubelet",
        "severity": "warning"
      },
      "annotations": {
        "description": "58.59% full for PVC data-pvc.",
        "summary": "PVC volume is full"
      },
      "startsAt": "2022-03-22T15:02:22.32Z",
      "endsAt": "2022-03-22T15:07:52.32Z",
      "generatorURL": "https://thanos-querier-openshift-monitoring.clu-jack-dev.cern.ch/graph?g0.expr=kubelet_volume_stats_used_bytes%7Bnamespace%3D%22test-pvc-alert%22%7D+%2F+kubelet_volume_stats_capacity_bytes%7Bnamespace%3D%22test-pvc-alert%22%7D+%2A+100+%3E+0.5&g0.tab=1",
      "fingerprint": "aef3ae93b3e21235"
    }
  ],
  "groupLabels": {
    "alertname": "UserVolumeUsageAlert"
  },
  "commonLabels": {
    "alertname": "UserVolumeUsageAlert",
    "endpoint": "https-metrics",
    "instance": "188.184.103.222:10250",
    "job": "kubelet",
    "metrics_path": "/metrics",
    "namespace": "test-pvc-alert",
    "node": "standard-56m6q",
    "persistentvolumeclaim": "data-pvc",
    "prometheus": "openshift-monitoring/k8s",
    "service": "kubelet",
    "severity": "warning"
  },
  "commonAnnotations": {
    "description": "58.59% full for PVC data-pvc.",
    "summary": "PVC volume is full"
  },
  "externalURL": "https://alertmanager-main-openshift-monitoring.clu-jack-dev.cern.ch",
  "version": "4",
  "groupKey": "{}/{alertname=\"UserVolumeUsageAlert\"}:{alertname=\"UserVolumeUsageAlert\"}",
  "truncatedAlerts": 0
}
EOF

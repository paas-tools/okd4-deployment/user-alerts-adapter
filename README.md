# User Alerts Adapter

Converts Alerts received through [Alertmanager Webhooks](https://www.prometheus.io/docs/alerting/latest/configuration/#webhook_config) into [CERN Notifications](https://notifications.web.cern.ch/).

Subscribe to [notifications-qa-users egroup](https://e-groups.cern.ch/) to get access to the [Notifications QA instance](https://notifications-qa.web.cern.ch) (search for the group name, then choose "Members" and click "Add me").

## Development

Run the server locally:

```sh
export NOTIFICATIONS_API_KEY=xxx
export NOTIFICATIONS_CHANNEL_ID=xxx
export NOTIFICATIONS_ENDPOINT=
go run . -kubeconfig $KUBECONFIG -v 3
```

Send a fake alert webhook:

```sh
./alert.sh
```

Build the image and push to Harbor (for development):

```sh
podman image build -t registry.cern.ch/jhensche/user-alerts-adapter:latest .
podman image push registry.cern.ch/jhensche/user-alerts-adapter:latest
```

To add or update a dependency, use `go get -u github.com/some/example/go/module && go mod tidy`.
Dependencies are [vendored](https://go.dev/ref/mod#go-mod-vendor) (copied) in this project.
After adding or updating dependency, use `go mod vendor && git add -f go.mod go.sum vendor` to add the dependencies to the repo.

## Metrics

The server exposes the following [Prometheus metrics](https://prometheus.io/docs/concepts/metric_types/) at the `/metrics` endpoint:

Name | Labels | Type | Description
-----|--------|------|------------
user_alerts_webhooks_received | status="ok|failed" | Counter | Total number of webhooks received from Alertmanager. Status "failed" means the server was unable to parse the request body.
user_alerts_alerts_processed | status="ok|failed|ignored", project=<PROJECT_NAME> | Counter | Total number of alerts received from Alertmanager (one webhook request may contain multiple alerts).
user_alerts_notifications_send | status="ok|failed", project=<PROJECT_NAME> | Counter | Total number of notifications send via the Notifications API

Note that metrics with zero values do not appear!


## References

* [CERN Notifications API docs](https://notifications-qa.web.cern.ch/swagger/)
* [Alertmanager webhook reference](https://www.prometheus.io/docs/alerting/latest/configuration/#webhook_config)

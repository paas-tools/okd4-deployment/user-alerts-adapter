package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/prometheus/alertmanager/template"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog"
)

const (
	labelNamespaceOwner      = "lifecycle.webservices.cern.ch/owner"
	labelNamespaceAdminGroup = "lifecycle.webservices.cern.ch/adminGroup"
)

type NotificationsConfig struct {
	apiKey      string
	channelName string
	channelId   string
	endpoint    string
}

type App struct {
	kubeClient          kubernetes.Clientset
	notificationsConfig NotificationsConfig
	httpClient          http.Client
}

// NewApp builds a new App structure in the "regular" way:
// it loads configuration from environment variables and initializes
// a proper kubernetes clientset
func NewApp(masterUrl string, kubeconfig string) (App, error) {
	app := App{}
	cfg, err := clientcmd.BuildConfigFromFlags(masterUrl, kubeconfig)
	if err != nil {
		return app, fmt.Errorf("Error building kubeconfig: %s", err.Error())
	}

	kubeClient, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return app, fmt.Errorf("Error building kubernetes clientset: %s", err.Error())
	}
	app.kubeClient = *kubeClient

	app.httpClient = http.Client{}

	app.notificationsConfig.apiKey = os.Getenv("NOTIFICATIONS_API_KEY")
	if app.notificationsConfig.apiKey == "" {
		return app, fmt.Errorf("Missing environment variable NOTIFICATIONS_API_KEY")
	}
	app.notificationsConfig.channelId = os.Getenv("NOTIFICATIONS_CHANNEL_ID")
	if app.notificationsConfig.channelId == "" {
		return app, fmt.Errorf("Missing environment variable NOTIFICATIONS_CHANNEL_ID")
	}
	app.notificationsConfig.endpoint = os.Getenv("NOTIFICATIONS_ENDPOINT")
	if app.notificationsConfig.endpoint == "" {
		app.notificationsConfig.endpoint = "https://notifications.web.cern.ch/api/notifications"
	}
	klog.V(3).Infof("Using CERN notifications endpoint '%s'", app.notificationsConfig.endpoint)

	return app, nil
}

// NewTestApp builds a mock App structure with a fake kubernetes clientset
func NewTestApp() (App, error) {
	// TODO
	return App{}, nil
}

func (app App) Run(addr string) error {
	http.HandleFunc("/alerts", app.alertsHandler)
	http.HandleFunc("/healthz", healthzHandler)
	http.Handle("/metrics", promhttp.Handler())

	klog.V(1).Infof("Listening on %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		return fmt.Errorf("Failed to start http server on %s: %v", addr, err)
	}
	return nil
}

func main() {
	// parse command-line arguments
	klog.InitFlags(nil)
	address := flag.String("address", ":8080", "address and port of service")
	kubeconfig := flag.String("kubeconfig", "", "Path to a kubeconfig. Only required if out-of-cluster.")
	masterUrl := flag.String("master", "", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	flag.Parse()

	// intitialize our data structures and load configuration from environment
	app, err := NewApp(*masterUrl, *kubeconfig)
	if err != nil {
		klog.Fatal(err)
	}

	// start our main process
	klog.Fatal(app.Run(*address))
}

func healthzHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

// alertsHandler represents the HTTP handler function that receives the webhook payload
// from Alertmanager, parses it and sends the notifications, if necessary.
func (app *App) alertsHandler(w http.ResponseWriter, r *http.Request) {
	var webhookData template.Data

	rawBody := bytes.NewBuffer([]byte{})
	err := json.NewDecoder(io.TeeReader(r.Body, rawBody)).Decode(&webhookData)
	if err != nil {
		klog.Errorf("Cannot parse content because of %s", err)
		webhooksReceived.With(prometheus.Labels{"status": "failed"}).Inc()
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	klog.V(5).Infof("Received webhook payload: %s", rawBody)
	webhooksReceived.With(prometheus.Labels{"status": "ok"}).Inc()

	// the externalURL sent by Alertmanager looks like this:
	// https://console-openshift-console.clu-jack-dev.cern.ch/monitoring
	// https://paas.cern.ch/monitoring
	baseUrl := strings.TrimSuffix(webhookData.ExternalURL, "/monitoring")
	alertStatus := webhookData.Status
	// loop over all alerts in the payload and handle them individually
	for _, alert := range webhookData.Alerts {
		// for now we ignore "resolved" alerts
		// in the future, we could do something smart with those
		project := alert.Labels["namespace"]
		if alertStatus == "firing" {
			notification, err := app.handleActiveAlert(alert, baseUrl)
			if err != nil {
				klog.Errorf("failed to handle alert: %s", err)
				alertsProcessed.With(prometheus.Labels{"status": "failed", "project": project}).Inc()
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			alertsProcessed.With(prometheus.Labels{"status": "ok", "project": project}).Inc()
			err = app.sendNotification(notification)
			if err != nil {
				klog.Errorf("failed to send notification: %s", err)
				notificationsSend.With(prometheus.Labels{"status": "failed", "project": project}).Inc()
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			notificationsSend.With(prometheus.Labels{"status": "ok", "project": project}).Inc()
		} else {
			klog.V(3).Infof("Ignoring unsupported alert status %s", alertStatus)
			alertsProcessed.With(prometheus.Labels{"status": "ignored", "project": project}).Inc()
		}
	}
	w.WriteHeader(http.StatusNoContent)
}

// handleActiveAlert takes a single AlertManager alert and processes it.
// It checks if notifications are enabled/disabled for this alert
// and retrieves the list of adminstrators (owner + project admins).
// Finally, it builds a Target Notification for the channel and returns it.
func (app App) handleActiveAlert(alert template.Alert, baseUrl string) (TargetedNotification, error) {
	klog.V(2).Infof("Processing alert '%s' for namespace '%s'", alert.Fingerprint, alert.Labels["namespace"])
	// the notification object we will send to the API
	// Summary and Body (HTML) will be filled later
	notification := TargetedNotification{
		Priority: "NORMAL",
		Source:   "API",
		Target:   app.notificationsConfig.channelId,
		Private:  true,
	}

	project := alert.Labels["namespace"]
	alertName := alert.Labels["alertname"]
	switch alertName {
	// the name of this alert is special
	// we send nice, user-friendly notficiations
	case "VolumeUsageAlert":
		pvcName := alert.Labels["persistentvolumeclaim"]
		usageRatioStr := alert.Labels["usageratio"]
		usageRatio, _ := strconv.ParseFloat(usageRatioStr, 64)

		notification.Summary = fmt.Sprintf("Volume %s in Openshift project %s almost full", pvcName, project)
		// TODO: the body should use a proper HTML template
		notification.Body = fmt.Sprintf("The PVC '%s' in the Openshift project '%s' is %.f%% full. Please expand the size of the PVC at %s/k8s/ns/%s/persistentvolumeclaims/%s",
			pvcName, project, usageRatio*100, baseUrl, project, pvcName)

	// in case none of the previous special names matches, we send fill the notification with a generic template based on labels and annotations
	default:

		notification.Summary = fmt.Sprintf("Alert '%s' in Openshift project is active", alertName)
		// TODO: the body should use a proper HTML template
		annotationsHtml := ""
		for k, v := range alert.Annotations {
			annotationsHtml += fmt.Sprintf("<br>%s: %s", k, v)
		}
		notification.Body = fmt.Sprintf("The alert '%s' in the Openshift project '%s' is active. %s", alertName, project, annotationsHtml)
	}

	// figure out the appropriate alert recipients
	adminUsers, adminGroups, err := app.getProjectAdmins(project)
	if err != nil {
		return notification, err
	}
	// convert the flat string arrays into the expected structures
	notification.TargetUsers = make([]TargetUserStruct, len(adminUsers))
	for i := range adminUsers {
		// Notifications API supports sending plain usernames (instead of e-mail addresses)
		notification.TargetUsers[i].Email = adminUsers[i]
	}
	notification.TargetGroups = make([]TargetGroupStruct, len(adminGroups))
	for i := range adminGroups {
		notification.TargetGroups[i].GroupIdentifier = adminGroups[i]
	}

	return notification, nil
}

func getPvcUrl(externalUrl string, namespace string, pvc string) string {
	baseUrl := strings.TrimSuffix(externalUrl, "/monitoring")
	fullUrl := fmt.Sprintf("%s/k8s/ns/%s/persistentvolumeclaims/%s", baseUrl, namespace, pvc)
	return fullUrl
}

// Returns the list of project administrators (owner + admins)
// split by users and groups, so it can be passed these separately to the notification API
func (app App) getProjectAdmins(name string) (users, groups []string, err error) {
	klog.V(3).Infof("Retrieving list of project administators for '%s'", name)

	// get owner from namespace label
	namespace, err := app.kubeClient.CoreV1().Namespaces().Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return users, groups, err
	}
	labels := namespace.GetLabels()
	klog.V(3).Infof("Retrieved labels for namespace '%s': %#v", name, labels)

	owner := labels[labelNamespaceOwner]
	if owner == "" {
		return users, groups, fmt.Errorf("Label '%s' on namespace '%s' does not contain valid owner", labelNamespaceOwner, namespace.Name)
	}
	users = []string{owner}

	adminGroup := labels[labelNamespaceAdminGroup]
	if adminGroup != "" {
		groups = []string{adminGroup}
	}

	// TODO: should we also look at the "admin" rolebinding?
	// possibly users could also create a "monitoring" rolebinding for this purpose
	// subjects:
	// - apiGroup: rbac.authorization.k8s.io
	//   kind: User
	//   name: xxxxx
	// - apiGroup: rbac.authorization.k8s.io
	//   kind: Group
	//   name: foobar-egroup

	klog.V(3).Infof("Found users %#v and groups %#v", users, groups)
	return users, groups, nil
}

// https://notifications.web.cern.ch/about#api
// https://notifications-qa.web.cern.ch/swagger/
type TargetUserStruct struct {
	Email string `json:"email"` // can be either username or e-mail address
}
type TargetGroupStruct struct {
	GroupIdentifier string `json:"groupIdentifier"`
}
type TargetedNotification struct {
	Target       string              `json:"target"`
	Summary      string              `json:"summary"`
	Priority     string              `json:"priority"` // CRITICAL, IMPORTANT, NORMAL, LOW
	Body         string              `json:"body"`
	Private      bool                `json:"private"`
	TargetUsers  []TargetUserStruct  `json:"targetUsers"`
	TargetGroups []TargetGroupStruct `json:"targetGroups"`
	TargetData   []string            `json:"targetData"`
	Source       string              `json:"source"`
}

func (app App) sendNotification(t TargetedNotification) error {
	payload, err := json.Marshal(t)
	if err != nil {
		return err
	}

	klog.V(3).Infof("Notification payload: %s", payload)
	req, err := http.NewRequest("POST", app.notificationsConfig.endpoint, bytes.NewReader(payload))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+app.notificationsConfig.apiKey)
	klog.V(5).Infof("HTTP request: %#v", req)
	resp, err := app.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	klog.V(3).Infof("Send notification result: %s %s", resp.Status, body)
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Notification API returned %s: %s", resp.Status, body)
	}

	klog.V(2).Infof("Successfully sent CERN notification ")
	return nil
}

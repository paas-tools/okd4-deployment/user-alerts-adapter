FROM docker.io/library/golang:1.20 as builder

WORKDIR /workspace
COPY . .

RUN CGO_ENABLED=0 go build -mod=vendor -o adapter

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/adapter .
USER nonroot:nonroot

ENTRYPOINT ["/adapter"]

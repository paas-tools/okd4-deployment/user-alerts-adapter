package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

// type metrics struct {
// 	webhooksReceived  *prometheus.CounterVec
// 	alertsProcessed   *prometheus.CounterVec
// 	notificationsSend *prometheus.CounterVec
// }

const metricsPrefix = "user_alerts_"

var webhooksReceived = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: metricsPrefix + "webhooks_received",
	Help: "Total number of HTTP webhook requests received from Alertmanager",
},
	[]string{"status"},
)

var alertsProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
	Name: metricsPrefix + "alerts_processed",
	Help: "Total number processed alert messages",
},
	[]string{"status", "project"},
)

var notificationsSend = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: metricsPrefix + "notifications_send",
		Help: "Total number of notifications send successfully via the Notifications API ",
	},
	[]string{"status", "project"},
)
